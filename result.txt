Welcome to Bulls and Cows
======================================
Please enter your secret code: (4 digits) 
2568
======================================
Do you want to: 1. use Keyboard, or 2. read from file? 2
Please enter the file name: guesses.txt
======================================
Ready? Let's go!
**********
Round 1
**********
Your guess: 1234
Result: 0 bulls and 3 cows
--------------------
Computer guess: 8073
Result: 0 bulls and 1 cow
**********
Round 2
**********
Your guess: 1345
Result: 0 bulls and 3 cows
--------------------
Computer guess: 7824
Result: 0 bulls and 2 cows
**********
Round 3
**********
Your guess: 1279
Result: 0 bulls and 2 cows
--------------------
Computer guess: 8632
Result: 0 bulls and 3 cows
**********
Round 4
**********
Your guess: 4281
Result: 1 bull and 1 cow
--------------------
Computer guess: 3207
Result: 0 bulls and 1 cow
**********
Round 5
**********
Your guess: 5678
Result: 0 bulls and 1 cow
--------------------
Computer guess: 8492
Result: 0 bulls and 2 cows
**********
Round 6
**********
Your guess: 9124
Result: 0 bulls and 2 cows
--------------------
Computer guess: 4619
Result: 0 bulls and 1 cow
**********
Round 7
**********
Your guess: 8967
Result: 0 bulls and 1 cow
--------------------
Computer guess: 4568
Result: 3 bulls and 0 cows
Draw!
======================================
Thank you for playing! Goodbye!
