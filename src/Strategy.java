import java.util.*;

/**
 * Created by yzhb363 on 22/05/2017.
 */
public class Strategy {

    private Set<String> possibleCodes;

    public Strategy() {
        this.possibleCodes = new HashSet<>();
        getPermutations(this.possibleCodes, "", "0123456789", 4);
    }

    private void getPermutations(Set<String> permutationSet, String possibleNum, String possibleDigitsString, int numLength) {
        int length = possibleNum.length();
        if (length == numLength) {
            permutationSet.add(possibleNum);
        } else {
            for (int i = 0; i < possibleDigitsString.length(); i++) {
                getPermutations(permutationSet, possibleNum + possibleDigitsString.charAt(i), possibleDigitsString.substring(0, i) + possibleDigitsString.substring(i + 1, possibleDigitsString.length()), length);
            }
        }
    }

    public String getRandomCode() {
        String randomCode = "";
        ArrayList<Integer> randomArray = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            randomArray.add(i);
        }

        Collections.shuffle(randomArray);

        for (int i = 0; i < 4; i++) {
            randomCode += randomArray.get(i);
        }
        return randomCode;
    }


    public String getCode() {
        return getRandomCode();
    }
}

