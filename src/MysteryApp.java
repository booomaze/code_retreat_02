import java.io.*;
import java.util.*;

public class MysteryApp {
    private static String output = "";
    private String secretCode = "";
    private int typeOfCode = 1;
    private String strategyCode = "numbers";
    private boolean readFromFile = false;
    private List<String> guesses = new ArrayList<>();
    private int count = 0;

    public static void saveOutput(String s) {
        System.out.println(s);
        output += s + "\n";
    }

    public void start() {

        showWelcome();
        getUserSecretNum();
        saveOutput("======================================");

        playOption();

        saveOutput("======================================");
        saveOutput("Ready? Let's go!");

        play();
        if (count == 7) {
            saveOutput("Draw!");
        }

        saveOutput("======================================");
        writeFile();
        saveOutput("Thank you for playing! Goodbye!");
    }


    public void showWelcome() {
        saveOutput("Welcome to Bulls and Cows");
        saveOutput("======================================");
    }


    public void getUserSecretNum() {

        while (true) {
            saveOutput("Please enter your secret code: (4 digits) ");
            secretCode = (new Scanner(System.in)).nextLine();
            output += secretCode + "\n"; // keep, don't delete
            if (useIt(secretCode, typeOfCode)) {
                break;
            }
            saveOutput("Invalid secret code.");
        }
    }

    public void playOption() {

        while (true) {
            saveOutput("Do you want to: 1. use Keyboard, or 2. read from file? ");
            try {
                int choice = Integer.parseInt((new Scanner(System.in)).nextLine());
                output += choice + "\n";
                if (choice == 1) {
                    break;
                }
                if (choice == 2) {
                    System.out.print("Please enter the file name: ");
                    output += "Please enter the file name: ";
                    String filename = (new Scanner(System.in)).nextLine();
                    output += filename + "\n";
                    guesses = new ArrayList<>();

                    try (BufferedReader bR = new BufferedReader(new FileReader(filename))) {
                        String guess;
                        while ((guess = bR.readLine()) != null) {
                            guesses.add(guess);
                        }
                    }

                    readFromFile = true;
                    break;
                }
            } catch (NumberFormatException e) {
                saveOutput("Invalid input. Please enter 1 or 2 only.");
            } catch (IOException e) {
                readFromFile = false;
                saveOutput("File cannot be found or something is wrong! Please try again.");
            }
        }
    }

    public void play() {

        Strategy strategy = new Strategy();
        Guesser computer = new Guesser("computer", null, strategy, strategyCode);
        Guesser player = new Guesser("human", secretCode, null, null);

        while (count < 7) {
            saveOutput("**********");
            saveOutput("Round " + (count + 1));
            saveOutput("**********");

            String guess = "";
            if (readFromFile) {
                guess = guesses.get(count);
                saveOutput(guess);
            } else {
                while (true) {
                    saveOutput("Your guess: ");
                    guess = (new Scanner(System.in)).nextLine();
                    output += guess + "\n"; // keep
                    if (useIt(guess, typeOfCode)) {
                        break;
                    }
                    saveOutput("Invalid guess");
                }
            }

            int[] playerMatch = computer.matchSecretCode(guess);
            if (playerMatch[0] == 4) {
                saveOutput("You win!");
            }

            String playerResult = player.informOfMatchResults(playerMatch);
            saveOutput(playerResult);
            saveOutput("--------------------");

            String computerGuess = computer.makeGuess();
            saveOutput("Computer guess: " + computerGuess);
            int[] computerMatch = player.matchSecretCode(computerGuess);
            if (computerMatch[0] == 4) {
                saveOutput("Computer win! :)");
                break;
            }
            String computerResult = computer.informOfMatchResults(computerMatch);
            saveOutput(computerResult);
            count++;
        }

    }

    public void writeFile() {

        while (true) {
            System.out.print("Do you want to save the results to a file? 1. Yes 2. No: "); // keep
            try {
                int choice = Integer.parseInt((new Scanner(System.in)).nextLine());
                if (choice == 1) {
                    System.out.print("Please enter a file name: "); // keep
                    String filename = (new Scanner(System.in)).nextLine();
                    try (BufferedWriter bW = new BufferedWriter(new FileWriter(filename))) {
                        bW.write(output);
                    }
                    break;
                }
                if (choice == 2) {
                    break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter 1 or 2 only."); // keep
            } catch (IOException e) {
                System.out.println("File not found. Please try again."); // keep
            }
        }
    }


    private static boolean useIt(String code, int typeOfCode) {
        String pattern = typeOfCode == 1 ? "([0-9]{4})$" : "([A-F]{4})$";
        return code.matches(pattern) && codeOk(code);
    }

    private static boolean codeOk(String code) {
        for (int i = 0; i < code.length(); i++) {
            for (int j = i + 1; j < code.length(); j++) {
                if (code.charAt(i) == code.charAt(j)) {
                    return false;
                }
            }
            return true;
        }
        return true;
    }

    public static void main(String[] args) {
        new MysteryApp().start();
    }
}



