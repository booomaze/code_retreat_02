/**
 * Created by yzhb363 on 22/05/2017.
 */
public class Guesser {


    private Strategy strategy;
    private char[] secretCode = new char[0];
    private String type;

    public Guesser(String type, String secretCode, Strategy strategy, String strategyCode) {
        this.type = type;
        if (type.equals("human")) {
            this.secretCode = secretCode.toCharArray();
        } else if (type.equals("computer")) {
            this.secretCode = strategy.getCode().toCharArray();
        }
        this.strategy = strategy;
    }

    public int[] matchSecretCode(String guess) {
        int bulls = 0;
        int cows = 0;
        for (int i = 0; i < secretCode.length; i++) {
            if (guess.charAt(i) == secretCode[i]) {
                bulls++;
            } else if (guess.contains(secretCode[i] + "")) {
                cows++;
            }
        }
        return new int[] {bulls, cows};
    }

    public String makeGuess() {
        return this.strategy.getCode();
    }

    public String informOfMatchResults(int[] match) {
        if (type.equals("human")) {
            return "Result: " + match[0] + (match[0] == 1 ? " bull" : " bulls") + " and " + match[1] + (match[1] == 1 ? " cow" : " cows");
        } else if (type.equals("computer")) {
            return "Result: " + match[0] + (match[0] == 1 ? " bull" : " bulls") + " and " + match[1] + (match[1] == 1 ? " cow" : " cows");
        }

        return null;
    }



}
